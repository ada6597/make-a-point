import { ActivatedRoute } from '@angular/router';
import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatSelectChange } from '@angular/material/select';
import { debounceTime, distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Users } from '../model/users-models';
import { UsersResolverService } from '../Resolvers/users-resolver.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit, AfterViewInit, OnDestroy {
  citiesList: { id: number; cityName: string }[] = [];
  filteredUsers: Users[] = [];
  name!: string[];
  protected destroyed$ = new Subject<void>();
  searchControl = new FormControl('');
  selectedCity = new FormControl('');
  users!: Users[];

  constructor(
    private _route: ActivatedRoute,
    public _userService: UsersResolverService
  ) {}

  ngAfterViewInit() {
    this.searchControl.valueChanges
      .pipe(
        debounceTime(400),
        distinctUntilChanged(),
        takeUntil(this.destroyed$)
      )
      .subscribe((v: string | null) => this.handleSearch(v));
  }

  ngOnInit(): void {
    this._route.data.subscribe((userContent) => {
      const { user } = userContent;
      this.users = user;
      this.citiesList = this.users.map((user, index) => {
        return {
          id: index,
          cityName: user.address.city,
        };
      }).sort((a,b)=> (a.cityName > b.cityName ? 1 : -1))
      ;
    });
    this.handleSearch('');
  }

  handleSearch(value:string |null): void {
    this.filteredUsers = value === '' ? this.users : this.filteredUser(value);
  }

  clearSearch() {
    this.searchControl.patchValue('');
  }

  citySelection(event: MatSelectChange): void {
    this.filteredUsers =
      event.value === '' ? this.users : this.filteredUser(event.value);
  }

  filterCondition = (condition: string, searchText?: string |null) => {
    return condition.toLowerCase().includes(searchText ? searchText.trim().toLowerCase() : '');
  };

  filteredUser = (searchText: string |null) => {
    return this.users.filter((user) => {
      return (
        this.filterCondition(user.name, searchText) ||
        this.filterCondition(user.company.name, searchText) ||
        this.filterCondition(user.address.city, searchText)
      );
    });
  };

  getLetters(userName: string) {
    const splitedName = userName.split('');
    return `${splitedName[0]}${splitedName[1].toUpperCase()}`;
  }

  ngOnDestroy() {
    this.destroyed$.next();
    this.destroyed$.complete();
  }
}
