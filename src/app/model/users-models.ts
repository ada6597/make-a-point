export interface Users {
    id: number;
    name: string;
    username: string;
    email: string;
    address: AddressDetail;
    phone: string;
    website: string;
    company: CompanyDetail
}

export interface AddressDetail {
    street: string;
    suite: string;
    city: string;
    zipcode: string;
    geo: GeoDetail;
}

export interface GeoDetail {
    lat: string;
    lng: string;
}

export interface CompanyDetail {
    name: string;
    catchPhrase: string;
    bs: string;
}