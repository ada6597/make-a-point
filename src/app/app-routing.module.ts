import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UsersComponent } from './users/users.component';
import { UsersResolverService } from './Resolvers/users-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: UsersComponent,
    resolve: { user: UsersResolverService }
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
