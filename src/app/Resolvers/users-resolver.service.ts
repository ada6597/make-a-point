import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { Users } from '../model/users-models';

@Injectable({
  providedIn: 'root'
})
export class UsersResolverService implements Resolve<Users[]>{

  constructor(private _http:HttpClient) {}

  resolve():Observable<Users[]>{
    const url='https://jsonplaceholder.typicode.com/users';
   return this._http.get<Users[]>(url)
  }
}
